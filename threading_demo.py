import hashlib
import multiprocessing
import string
import random
import threading
from queue import Queue
import time

# Linkedin password checker: https://lastpass.com/linkedin/
# 10M passwords: https://xato.net/today-i-am-releasing-ten-million-passwords-b6278bbe7495

ROUNDS = 10


def get_list_of_random_strings(n):
    l = list()
    for i in range(1, n):
        s = ''.join([random.choice(string.ascii_letters + string.digits) for _ in range(100)])
        l.append(s.encode('utf-8'))
    return l


def get_list_of_passwords(cnt):
    p = list()
    with open('data/ranked_300k.txt', 'r') as file:
        counter = 0
        for line in file:
            p.append(line.encode('utf-8'))
            counter += 1
            if counter >= cnt:
                break
    return p


def serial_list(clear_text, worker_cnt):
    hashed_text = list()
    for s in clear_text:
        s = s.strip()
        h = str
        for i in range(ROUNDS):
            h = hashlib.sha1(s).hexdigest()
        hash_as_s = str(h)
        hashed_text.append((hash_as_s, s))
    return hashed_text


def serial_queue(clear_text: list, worker_cnt):
    que = Queue(len(clear_text))
    _serial_que_helper(clear_text, que, worker_cnt)
    que.task_done()
    return que.queue


def _serial_que_helper(clear_text: list, results: Queue, worker_cnt):
    for s in clear_text:
        s = s.strip()
        h = str
        for i in range(ROUNDS):
            h = hashlib.sha1(s).hexdigest()
        hash_as_s = str(h)
        results.put((hash_as_s, s))


def hash_threads(clear_text, worker_cnt=1):
    qeu = Queue(len(clear_text))
    workers = list()
    start = 0
    step = len(clear_text) // worker_cnt
    stop = start + step
    for i in range(worker_cnt):
        w = threading.Thread(target=_serial_que_helper, args=(clear_text[start:stop], qeu, None))
        workers.append(w)
        start = stop
        stop = stop + step

    for w in workers:
        w.start()

    for w in workers:
        w.join()

    result = qeu.queue
    return result


def proc_consumer_queue(name: str, task_queue: multiprocessing.JoinableQueue, result_queue: multiprocessing.Queue):
    while True:
        text = task_queue.get()
        if text is None:
            # print("{} quiting".format(name))
            task_queue.task_done()
            break
        for i in range(ROUNDS):
            hash_text = hashlib.sha1(text).hexdigest()
            task_queue.task_done()
            result_queue.put((str(hash_text), text))


def proc_provider_queue(clear_text: list, task_queue: multiprocessing.JoinableQueue, consumer_cnt):
    for text in clear_text:
        task_queue.put(text.strip())
    for cnt in range(consumer_cnt):
        task_queue.put(None)


def multiproc_que(clear_text, worker_cnt=1):
    tasks = multiprocessing.JoinableQueue(len(clear_text))
    results = multiprocessing.Queue()
    processors = list()
    hashed_text = list()

    for p in range(worker_cnt):
        name = "worker-{}".format(p)
        p = multiprocessing.Process(target=proc_consumer_queue, args=(name, tasks, results))
        processors.append(p)
        p.start()

    for text in clear_text:
        text = text.strip()
        tasks.put(text)
        if not results.empty():
            hashed_text.append(results.get())

    # Send a poison pill to each worker
    for p in range(worker_cnt):
        tasks.put(None)

    tasks.join()

    while not results.empty():
        hashed_text.append(results.get())

    return hashed_text


def proc_consumer_pool(clear_text: str):
    clear_text = clear_text.strip()
    h = str
    for i in range(ROUNDS):
        h = hashlib.sha1(clear_text).hexdigest()
    return (h, clear_text)

def multiproc_pool(clear_text, worker_cnt=1):
    p = multiprocessing.Pool(worker_cnt)
    hashed_text = p.map(proc_consumer_pool, clear_text)
    p.close()
    p.join()
    return hashed_text


def time_it(my_hasher, clear_text: list, out_file_name=None, worker_cnt=None):
    time_start = time.time()
    hashed_strings = my_hasher(clear_text, worker_cnt=worker_cnt)
    time_stop = time.time()
    time_elapsed = time_stop - time_start

    # multiprocessing.Queue is not iterable, so manually convert to list
    # try:
    #     _ = iter(hashed_strings)
    # except TypeError:
    #     hashed_strings_list = list()
    #     while not hashed_strings.empty():
    #         hashed_strings_list.append(hashed_strings.get())
    #     hashed_strings = hashed_strings_list

    if out_file_name:
        with open(out_file_name, 'w') as f:
            for h in sorted(hashed_strings):  # sort so we can diff the output files
                f.write("{} {}\n".format(h[0], h[1]))

    return time_elapsed, hashed_strings


if __name__ == '__main__':
    # clear_text = get_list_of_passwords(1000)
    # t0 = time.time()
    # hashed_passwords = hash_serial(clear_text)
    # t1 = time.time()
    # with open('out_serial.txt', 'w') as f:
    #     for h in hashed_passwords:
    #         f.write(h + '\n')
    # print("Serial: {}".format(round(t1 - t0, 5)))

    hash_func = [
        (serial_list, None),
        (serial_queue, None),
        (hash_threads, 1),
        (hash_threads, 2),
        (hash_threads, 4),
        # (multiproc_que, 1),  ####
        # (multiproc_que, 2),  ####
        # (multiproc_que, 4),  ####
        (multiproc_pool, 1),
        (multiproc_pool, 2),
        (multiproc_pool, 4),
    ]

    # clear_text = get_list_of_passwords(1000)
    # print(f"Calculating {len(clear_text)} hashes")
    # for f in hash_func:
    #     func_name = str(f[0].__name__)
    #     out_file_name = 'out_{}_{}.txt'.format(func_name, f[1])
    #     t, hashed_passwords = time_it(f[0], clear_text, out_file_name, f[1])
    #     print(f"{func_name}:\t\t{round(t, 3)} seconds.")

    # There as to be a bette way to do this.
    columns = []
    tot_text = 362016
    # tot_text = 1000
    data_point_cnt = 10
    step = tot_text // data_point_cnt
    col_cnt = data_point_cnt
    while col_cnt < tot_text:
        columns.append(col_cnt)
        col_cnt += step

    with open("stats.csv", 'w') as stats:
        stats.write("Function, ")
        for c in columns:
            stats.write(f"{c}, ")
        stats.write('\n')
        for f in hash_func:
            func_name = str(f[0].__name__) + "_" + str(f[1])
            print(f"Starting {func_name}\n")
            stats.write(f"{func_name},")
            for c in columns:
                clear_text = get_list_of_passwords(c)
                t, hashed_passwords = time_it(f[0], clear_text, f"{func_name}.txt", f[1])
                stats.write(f"{round(t,5)},")
            stats.write('\n')
