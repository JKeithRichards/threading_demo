#!/usr/bin/env python3

with open("messy.txt", 'r') as infile, open("clean.txt", 'w') as outfile:
    for l in infile:
        line = l.split()
        for w in line:
            outfile.write(w + '\n')
