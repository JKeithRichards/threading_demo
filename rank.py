#!/usr/bin/env python3
from collections import Counter

# Source of 10M passwords torrent: https://xato.net/today-i-am-releasing-ten-million-passwords-b6278bbe7495

passwords = list()
with open('10-million-combos.txt', 'r') as f:
    try:
        for line in f:
            try:
                d = line.split()
            except UnicodeDecodeError as e:
                print("splitting: {}".format(e))
            try:
                passwords.append(d[1])
            except IndexError as e:
                print("appending: {}".format(e))
    except UnicodeDecodeError as e:
        print("Reading: {}".format(e))

c = Counter(passwords)
ranked_pass = c.most_common(500000)

with open('ranked.txt', 'w') as f:
    for p in ranked_pass:
        f.write(p[0] + '\n')
